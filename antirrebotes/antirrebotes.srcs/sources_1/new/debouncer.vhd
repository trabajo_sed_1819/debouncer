----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 28.12.2018 20:48:20
-- Design Name: 
-- Module Name: debouncer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity debouncer is
port(
  clk: in std_logic;
  btn_in: in std_logic;
  btn_out: out std_logic
  );
  
end debouncer;

architecture Behavioral of debouncer is
constant cnt_size:integer:=6;
signal btn_prev: std_logic:='0';
signal cnt: std_logic_vector(cnt_size downto 0):=(others=>'0');
begin
 process(clk)
  begin
   if (clk'event and clk='1') then
    if (btn_prev xor btn_in)='1' then
     cnt<=(others=>'0');
     
     btn_prev<=btn_in;
     elsif (cnt(cnt_size)='0') then
      cnt<=cnt + 1;
      else
       btn_out<=btn_prev;
      end if;
      
     end if;
     end process;
     
       
     

end Behavioral;
